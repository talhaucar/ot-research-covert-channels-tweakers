# AES 256 encryption/decryption using pycryptodome library

from base64 import b64encode, b64decode
import hashlib
from Cryptodome.Cipher import AES
import os
from Cryptodome.Random import get_random_bytes

BLOCK_SIZE = 16
pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]


def encrypt(plain_text, password):

    private_key = hashlib.sha256(password.encode("utf-8")).digest()
    plain_text = pad(plain_text)
    print(plain_text)

    # create cipher config 
    cipher_config = AES.new(private_key, AES.MODE_ECB)

    # return a dictionary with the encrypted text
    cipher_text = cipher_config.encrypt(bytes(plain_text, 'utf-8'))

    return b64encode(cipher_text).decode('utf-8')
    


def decrypt(ciphertextEncoded, password):
    # decode the dictionary entries from base64
    cipher_text = b64decode(ciphertextEncoded)
    
    private_key = hashlib.sha256(password.encode("utf-8")).digest()

    # create the cipher config
    cipher = AES.new(private_key, AES.MODE_ECB)

    # decrypt the cipher text
    decrypted = cipher.decrypt(cipher_text)

    return unpad(decrypted)

